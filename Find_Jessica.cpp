#include <bits/stdc++.h>
#include <string.h>
using namespace std;

int main()
{
    string s;
    getline(cin, s);
    size_t pos = s.find("Jessica");
    // cout<<s.find("Jessica"); 
    // cout<<pos <<endl;

    if (pos != -1 && (pos == 0 || s[pos - 1] == ' ') && (pos + 7 == s.length() || s[pos + 7] == ' ') && (pos!=1 || s[7-pos] == ' '))
        cout << "YES" << endl;
    else
        cout << "NO" << endl;

    return 0;
}
