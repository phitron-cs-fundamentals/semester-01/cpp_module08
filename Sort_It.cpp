#include <bits/stdc++.h>
using namespace std;

class Student
{
public:
    string nm, cls, s;
    int id, math_marks, eng_marks;
};

bool compareStudents(Student a, Student b)
{
    int total_marks_A = a.math_marks + a.eng_marks;
    int total_marks_B = b.math_marks + b.eng_marks;
    if (total_marks_A != total_marks_B)
        return total_marks_A > total_marks_B;
    else
        return a.id < b.id;
}

int main()
{
    int N;
    cin >> N;
    Student *a = new Student[N];

    for (int i = 0; i < N; i++)
    {
        cin >> a[i].nm >> a[i].cls >> a[i].s >> a[i].id >> a[i].math_marks >> a[i].eng_marks;
    }

    sort(a, a + N, compareStudents);

    for (int i = 0; i < N; ++i)
    {
        cout << a[i].nm << " " << a[i].cls << " " << a[i].s << " " << a[i].id << " " << a[i].math_marks << " " << a[i].eng_marks << endl;
    }

    return 0;
}